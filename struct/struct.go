package main

import (
	"fmt"
)

//Doctor es la estructura para inicializar la collection del mismo
type Doctor struct {
	number     int
	name       string
	companions []string
}

//Animal es la estructura principal
type Animal struct {
	name   string //`required max:"100"`
	origin string
}

//Bird es la estructura que recibe  y hereda de animal
type Bird struct {
	Animal
	speedKPH float32
	canFly   bool
}

func main() {
	firstDoctor := Doctor{
		number: 1,
		name:   "Jon Pertwee",
		companions: []string{
			"Liz Shaw",
			"Jo Grant",
			"Jane Smith",
		},
	}

	fmt.Println(firstDoctor)
	fmt.Println(firstDoctor.name)
	fmt.Println(firstDoctor.companions[1])

	//Otra forma de crear struct pero no es recomendable debido a que no es muy legible
	goku := struct{ name string }{name: "Goku"}
	fmt.Println(goku)

	//de igual forma se puede hacer esto
	vegeta := goku
	vegeta.name = "Vegeta"
	fmt.Println(goku)
	fmt.Println(vegeta)

	// Probando la herencia en golang
	emu := Bird{
		speedKPH: 48.5,
		canFly:   false,
	}

	emu.name = "Emu"
	emu.origin = "Australia"

	fmt.Println(emu)

	// otra ave
	gallina := Bird{
		Animal: Animal{
			name:   "Gallina",
			origin: "World",
		},
		speedKPH: 20,
		canFly:   false,
	}

	fmt.Println(gallina)

	// t := reflect.TypeOf(Animal{})
	// field, _ := t.FieldByName("name")
	// fmt.Println(field.Tag)
}
