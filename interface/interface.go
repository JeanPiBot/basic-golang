package main

import "fmt"

func main() {
	var w Writer = ConsoleWriter{}
	w.Write([]byte("Hello world"))
}

//Writer is a blablabla
type Writer interface {
	Write([]byte) (int, error)
}

//ConsoleWriter is blablabalabla
type ConsoleWriter struct{}

func (cw ConsoleWriter) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data))
	return n, err
}
