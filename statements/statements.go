package main

import (
	"fmt"
)

func main() {
	// statePopulations := make(map[string]int)
	// statePopulations = map[string]int{
	// 	"Bogota":    7413000000,
	// 	"Medellin":  2427000000,
	// 	"Cali":      2228000000,
	// 	"Neiva":     357392,
	// 	"Florencia": 168346,
	// }

	// if pop, ok := statePopulations["Cali"]; ok {
	// 	fmt.Println(pop)
	// }

	number := 50
	guess := 30

	if guess < number {
		fmt.Println("Too Low")
	} else if guess > number {
		fmt.Println("Too High")
	} else if guess == number {
		fmt.Println("You got it")
	}

	//switch cases
	// switch 2 {
	// case 1, 5, 10:
	// 	fmt.Println("one")
	// case 2, 4, 9:
	// 	fmt.Println("two")
	// default:
	// 	fmt.Println("not one or two")
	// }

	i := 10
	switch {
	case i <= 10:
		fmt.Println("Less than or equal to ten")
	case i <= 20:
		fmt.Println("less than or equal to twenty")
	default:
		fmt.Println("Greater than twenty")
	}

	// para evaluar que tipo es
	var j interface{} = 30
	switch j.(type) {
	case int:
		fmt.Println("j is an int")
		// si queremos que no se ejecute lo que sigue en este código podremos con el break
		// break
		// fmt.Println("otro código de ejecución")
	case float64:
		fmt.Println("j is a float64")
	case string:
		fmt.Println("j is a string")
	default:
		fmt.Println("j is another type")
	}
}
