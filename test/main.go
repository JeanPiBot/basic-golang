package main

import (
	"fmt"
)

func main() {
	fmt.Println(suma(5, 5))
	fmt.Println(resta(3, 2))
}

func suma(number1, number2 int) int {
	return number1 + number2
}

func resta(number1, number2 int) int {
	return number1 - number2
}
