package main

import "testing"

func TestSuma(t *testing.T) {
	totalSuma := suma(5, 5)
	if totalSuma != 10 {
		t.Errorf("La suma es incorrecta, debido a que se obtuvo %d, y esperamos: %d", totalSuma, 10)
	}
}

func TestResta(t *testing.T) {
	totalResta := resta(3, 2)
	if totalResta != 1 {
		t.Errorf("La resta es incorrecta, debido a que se obtuvo %d, y esperamos: %d", totalResta, 1)
	}
}
