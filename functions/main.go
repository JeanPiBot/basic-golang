package main

import "fmt"

func main() {
	// for i := 0; i < 5; i++ {
	// 	Funcion("Hello World", i)
	// }

	// greeting := "Hello"
	// name := "Jean Pierre"

	// sayGreeting(greeting, name)

	// pasandole datos
	g := greeter{
		greeting: "Hello",
		name:     "World",
	}
	// llamando la funcion greet que esta en funcion
	g.greet()

	s := Suma(1, 2, 3, 4, 5)
	fmt.Println("The sum is ", s)

	d, err := Divide(5.0, 3.0)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(d)

	// funciones anonimas con su estructura
	func() {
		fmt.Println("Hello Go!")
	}()
}
