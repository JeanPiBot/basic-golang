package main

import "fmt"

type greeter struct {
	greeting string
	name     string
}

//Funcion es para probar las funciones de golang
// func Funcion(msg string, idx int) {
// 	fmt.Println(msg)
// 	fmt.Println("The value of the index is ", idx)
// }

// func sayGreeting(greeting, name string) {
// 	fmt.Println(greeting, name)
// }

func (g *greeter) greet() {
	fmt.Println(g.greeting, g.name)
}
