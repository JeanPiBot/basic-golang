package main

import (
	"fmt"
)

func main() {
	statePopulations := make(map[string]int, 10)
	// Aqui vamos a prender que es maps
	statePopulations = map[string]int{
		"Bogota":    7413000000,
		"Medellin":  2427000000,
		"Cali":      2228000000,
		"Neiva":     357392,
		"Florencia": 168346,
	}
	// esta es otra forma de iniciarlizarlo o crear un map
	// diaSemana := map[int]string{
	// 	1: "Lunes",
	// 	2: "Martes",
	// 	3: "Miercoles",
	// 	4: "Jueves",
	// 	5: "Viernes",
	// 	6: "Sabado",
	// 	7: "Domingo",
	// }

	// for dia := range diaSemana {
	// 	fmt.Println("El día", dia, "de la semana es", diaSemana[dia])
	// }

	fmt.Println(statePopulations)

	//para eliminar un elemento de nuestro map
	delete(statePopulations, "Cali")
	fmt.Println(statePopulations)

	//para agregar un elemento al map
	// statePopulations["Cali"] = 2228000000
	// fmt.Println(statePopulations)

	// otra forma de consultar los elementos del map
	pop, ok := statePopulations["Cali"]
	fmt.Println(pop, ok)
	pop, ok = statePopulations["Neiva"]
	fmt.Println(pop, ok)

	// referenciar en otro map
	sp := statePopulations
	delete(sp, "Neiva")
	fmt.Println(sp)
	fmt.Println(statePopulations)
}
