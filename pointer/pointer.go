package main

import "fmt"

type myStruct struct {
	foo int
}

//Pointer funcion nos va a enseñar como definir y utilizar los pointers
func Pointer() {
	//se define un número con un valor
	// number := 42
	// /* cuando creamos una variable y le asignamos el valor de number
	// lo que en realidad sucede es que solo copia ese valor
	// */
	// number2 := number
	// fmt.Println(number, number2)
	// // como podemos ver cuando le cambiamos el valor a number, no cambia su valor en number2
	// number = 27
	// fmt.Println(number, number2)

	// //para solucionar este problema lo podemos realizar de la siguiente manera
	// var number int = 42
	// var number2 *int = &number
	// // si no se pone el * mostrara el valor en memoria de nuestra variable number
	// fmt.Println(number, *number2)
	// fmt.Println(number2)

	// number = 57
	// fmt.Println(number, *number2)

	// //de igual manera si queremos cambiar el valor de las variables desde number2 lo podemos hacer
	// *number2 = 13
	// fmt.Println(number, *number2)

	var ms *myStruct
	// esta es otra forma de iniciarlizar e utilizar los pointers
	var ms2 *myStruct
	fmt.Println(ms)
	ms2 = new(myStruct)
	(*ms2).foo = 23
	ms = &myStruct{foo: 42}
	fmt.Println(ms)
	fmt.Println((*ms2).foo)

}
