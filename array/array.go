package main

import "fmt"

func main() {
	//esta forma no es muy conveniente
	grades := [3]int{97, 85, 93}
	// otra forma de hacerlo sin conocer el numero
	valores := [...]int{1, 2, 3, 4}
	fmt.Printf("Los valores: %v\n", valores)
	// para agregar en otra array se realiza de la siguiente manera
	b := &valores
	// de igual forma se puede cambiar valores
	b[1] = 6
	// slices
	c := b[1:] // slice from 4th element to end

	fmt.Printf("Grades: %v\n", grades)
	fmt.Printf("Length of values: %v\n", len(valores))
	fmt.Printf("Capacity of values: %v\n", cap(valores))
	fmt.Println(valores)
	fmt.Println(b)
	fmt.Println(c)
}
