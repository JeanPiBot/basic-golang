package main

func main() {
	//para convertir en una goroutine se hace agregando go
	// go sayHello()
	// //para mostrar en consola debemos agregar un retardo y no es una buena practica para production
	// time.Sleep(300 * time.Millisecond)

	/*
		otra forma de realizar con funciones anonimas
		pero esto tiene un problema cuando agregamos una variable fuera de la funcion anonima
		no se impreme lo que tiene dentro
		para solucionar este problema se puede realizar agregando parametroa la funcion
		sin embargo no mostrara el mensaje msg = goodbye
	*/
	// var msg = "Hola Jean P"
	// // esta es la forma de utilizar la espera de manera adecuada
	// wg.Add(1)
	// go func(msg string) {
	// 	fmt.Println(msg)
	// 	wg.Done()
	// }(msg)
	// msg = "GoodBye"
	// wg.Wait()

	Goroutine()

	// crear el numero de threads
	// runtime.GOMAXPROCS(100)
	// // estes es muy importante cuando hay muchos datos en el proceso
	// fmt.Printf("Threads: %v\n", runtime.GOMAXPROCS(-1))
}
