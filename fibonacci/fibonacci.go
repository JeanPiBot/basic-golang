package main

import "fmt"

func fibonacci() func() int {
	valorInicial, valorFinal := 0, 1
	return func() int {
		valorInicial, valorFinal = valorFinal, valorInicial+valorFinal
		return valorInicial
	}
}

func main() {
	var valorCalcular int
	funcion := fibonacci()

	fmt.Print("Escribe el valor al cual deseas calcular el fibonacci:")
	fmt.Scanf("%d", &valorCalcular)

	for i := 0; i < valorCalcular; i++ {
		fmt.Println(funcion())
	}
}
