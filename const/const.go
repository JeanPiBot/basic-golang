package main

import "fmt"

const (
	catSpecialist = iota
	dogSpecialist
	snakeSpecialist
)

const (
	_  = iota // ignore this
	KB = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)

func main() {
	const number = 42
	fmt.Printf("%v, %T\n", number, number)
	var specialistType int
	fmt.Printf("%v\n", specialistType == catSpecialist)
	fileSize := 4000000000.
	fmt.Printf("%.2fGB", fileSize/GB)
}
