package main

import (
	"fmt"
	"log"
)

// Panic nos ayuda a informarnos cuando no debemos continuar con una funcion
func Panic() {
	// este parte del código nos dice que no se puede dividir por cero por tal motivo da un panic
	// a, b := 1, 0
	// ans := a / b
	// fmt.Println(ans)

	// simple example para lo mismo
	fmt.Println("Start")
	defer fmt.Println("this was deferred")
	panicker()
	fmt.Println("end")

	// Hay situaciones en los cuales se debe evaluar si usar panic o no
	// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	w.Write([]byte("Hello Go!"))
	// })
	// err := http.ListenAndServe(":8080", nil)
	// if err != nil {
	// 	panic(err.Error())
	// }
}

func panicker() {
	fmt.Println("about to panic")
	//anonima funcion
	defer func() {
		if err := recover(); err != nil {
			log.Println("Error: ", err)
			panic(err)
		}
	}()
	panic("Something bad happened")
	fmt.Println("panic ending")

}
