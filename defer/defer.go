package main

import "fmt"

//Defer es solo una funcion de prueba para aprender sobre defer
func Defer() {
	fmt.Println("start")
	// con la palabra defer hacemos que se lea esa linea de código al final de la siguiente sentencia
	defer fmt.Println("middle")
	fmt.Println("end")

	a := "start"
	defer fmt.Println(a)
	a = "end"
}
